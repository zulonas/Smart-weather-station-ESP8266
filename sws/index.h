const char MAIN_page[] PROGMEM = R"=====(
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>SWS</title>
    <style>

    * {
        padding: 0px;
        margin: 0;
    }

    body{
      background-color: grey;
    }

    header {
      width: 100%;
      height: 300px;
      text-align: center;
      background-color: azure;
    }

    iframe{
        border: 1px solid black;
        padding: 2px;
        margin: 5px;
    }

    .weat_data{
      display: inline-block; 
      width: 190px;
      height: 75px;
      margin: 80px 15px;
      background-color: #33adff;
    }

    div.weat_data {
        color: white;
        font-family: 'PT Sans', sans-serif;
        font-weight: bold;
        font-size: 30px;
        line-height: 70px;
    }

    #main{
      text-align: center;
      padding-top: 20px
    }

    #main iframe{
      margin-bottom: 10px;
      display: inline-block;
      margin: 0px 50px;
    }

    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
  </head>
  <body>
    <header>
      <div class="weat_data" id="tmp1">18.50</div>
      <div class="weat_data" id="tmp2">18.50</div>
      <div class="weat_data" id="bmp">100477.39</div>
    </header>

    <div id="main">
      <iframe  id="tmp" width="474" height="293" seamless frameborder="0" scrolling="no" src=";format=interactive"></iframe>

      <iframe  id="bmp" width="474" height="293" seamless frameborder="0" scrolling="no" src=";format=interactive"></iframe>
    </div>
  <script>
  
  	function getxml(){
	  	var request = new XMLHttpRequest();
		request.open("GET", "xml", false);
		request.send();
		var xml = request.responseXML;
		xmlParser(xml);
	}

    function xmlParser(xml){
		document.getElementById("tmp1").innerHTML = xml.getElementsByTagName("data_tmp1")[0].textContent;
		document.getElementById("tmp2").innerHTML = xml.getElementsByTagName("data_tmp2")[0].textContent;
		document.getElementById("bmp").innerHTML = xml.getElementsByTagName("data_bmp")[0].textContent;
    }

    //$(document).ready(getxml);
    getxml();
	setInterval(getxml, 500);

  </script>
  </body>
</html>
)=====";
