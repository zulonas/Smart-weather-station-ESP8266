#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_BMP280.h>
#include <WiFiClientSecure.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include "index.h"
WiFiClientSecure client;


ESP8266WebServer server(80);

const char* ssid = "";
const char* password = "";

#define ONE_WIRE_PIN 2 
OneWire oneWire(ONE_WIRE_PIN); // d4
Adafruit_BMP280 bmp; // sda - d2, scl - d1
DallasTemperature sensors(&oneWire); 
//TMP address
DeviceAddress tmpsensor_1 = { 0x28, 0xFF, 0x0A, 0x83, 0xA6, 0x16, 0x03, 0x27 };
DeviceAddress tmpsensor_2 = { 0x28, 0xFF, 0x56, 0x3B, 0xA6, 0x16, 0x05, 0xC3 };
//BMP address
DeviceAddress bmpsensor_1 = {0x76};

String XML, PostData;
unsigned long previousMillis = 0;
const long interval = 60000;


/*---------------------------------------------------------------------*/

void main_content() {
  server.send(200, "text/html", MAIN_page);
}

void xml_content() {

  XML = "<?xml version='1.0'?> ";
  XML += "<variables>";
  XML += "<data_tmp1>";
  XML += sensors.getTempC(tmpsensor_1);
  XML += "</data_tmp1> ";
  XML += "<data_tmp2>";
  XML += sensors.getTempC(tmpsensor_2);
  XML += "</data_tmp2> ";
  XML += "<data_bmp>";
  XML += bmp.readPressure();
  XML += "</data_bmp>";
  XML += "</variables>";

  server.send(200, "text/xml", XML);
}

void to_google_sheets() {

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval && WiFi.status()== WL_CONNECTED) {
    previousMillis = currentMillis;

    PostData = "?tmp1=";
    PostData += sensors.getTempC(tmpsensor_1);
    PostData += "&tmp2=";
    PostData += sensors.getTempC(tmpsensor_2);
    PostData += "&bmp=";
    PostData += bmp.readPressure();
    
    PostData.replace(".", ",");

    if(client.connect("script.google.com", 443)){
      client.println("GET /macros/s/**************************************/exec" + PostData + " HTTP/1.1");
      client.println("Host: script.google.com");
      client.println("Connection: close");
      client.println();
      
    }  
  }
}

void setup() {

  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  Serial.println(WiFi.localIP());

  //TMP
  sensors.begin();
  sensors.setResolution(tmpsensor_1, 10);
  sensors.setResolution(tmpsensor_2, 10);

  //BMP
  Wire.begin(4, 5);
  if (!bmp.begin(0x76)) {
    Serial.println("BMP sensor error");
    while (1);
  }

  server.on("/", main_content);
  server.on("/xml", xml_content);
  server.begin();

}

void loop() {

  sensors.requestTemperatures();
  server.handleClient();
  to_google_sheets();
}
