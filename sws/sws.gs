// original from: http://mashe.hawksey.info/2014/07/google-sheets-as-a-database-insert-with-apps-script-using-postget-methods-with-ajax-example/
//resourse https://gist.github.com/willpatera/ee41ae374d3c9839c2d6

//var SHEET_NAME = "Sheet1";
var SHEET_ID = "";


var SCRIPT_PROP = PropertiesService.getScriptProperties();

function doGet(e){
  return handleResponse(e);
}

function doPost(e){
  return handleResponse(e);
}

function handleResponse(e) {
  
  var lock = LockService.getPublicLock();
  lock.waitLock(30000);  // wait 30 seconds before conceding defeat.
  
  try {
    var doc = SpreadsheetApp.openById(SHEET_ID); 
    
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getSheets()[0];
    
    var headers = sheet.getSheetValues(1, 1, 1, 4);
    
    var nextRow = sheet.getLastRow()+1; // get next row
    
    sheet.getRange(nextRow, 1, 1, 1).setValue(new Date()); //date
    sheet.getRange(nextRow, 2, 1, 1).setValue(e.parameter.tmp1); //tmp1
    sheet.getRange(nextRow, 3, 1, 1).setValue(e.parameter.tmp2); //tmp2
    sheet.getRange(nextRow, 4, 1, 1).setValue(e.parameter.bmp); //bmp
    
    //https://developers.google.com/apps-script/guides/web#deploying_a_script_as_a_web_app
    
    return ContentService
          .createTextOutput(JSON.stringify({"result":"success", "row": nextRow}))
          .setMimeType(ContentService.MimeType.JSON);
  } catch(e){ // if error return this
    
    return ContentService
          .createTextOutput(JSON.stringify({"result":"error", "error": e}))
          .setMimeType(ContentService.MimeType.JSON);
  }
  finally { //release lock
    lock.releaseLock();
  }
}

function setup(e){
    var doc = SpreadsheetApp.getActiveSpreadsheet();
    SCRIPT_PROP.setProperty("key", doc.getId());
}


function deleteold(e){
  var past = subDaysFromDate(new Date(),7);
  
  var doc = SpreadsheetApp.openById(SHEET_ID); 
  var sheet = SpreadsheetApp.getActiveSheet();
  var data = sheet.getDataRange().getValues();
  
 
  for (var i = 2; i < data.length && data[i][0] != ''; ++i) {
    if(data[i][0]==undefined){
      continue;
    }
    else if(data[i][0]<past){
      sheet.deleteRow(i);
    }
  }
} 

function subDaysFromDate(date,d){
  // d = number of day ro substract and date = start date
  var result = new Date(date.getTime()-d*(24*3600*1000));
  return result
}
