# Smart weather station with an ESP8266

Project with an ESP8266 - Smart weather station

## Schematics
Hardware:
* ESP8266
* BMP2B80
* 2xDS18B20+
![smart-weather-station](https://user-images.githubusercontent.com/24975714/33530228-47c96664-d885-11e7-8c6f-605f8656d06f.png)

